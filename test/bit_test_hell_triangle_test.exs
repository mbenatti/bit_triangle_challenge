defmodule Bit.Tests.TriangleTest do
  use ExUnit.Case
  doctest Bit.Tests.Triangle

  alias Bit.Tests.Triangle

  test "test1" do
    assert 26 == Triangle.triangle_challenge [[6],[3,5],[9,7,1],[4,6,8,4]]
  end

  test "test2" do
    assert 28 == Triangle.triangle_challenge [[6],[3,5],[9,7,1],[4,6,8,4],[5,9,1,2,3,4]]
  end

  test "test3" do
    assert 15 == Triangle.triangle_challenge [[1],[2,4],[9,2,3],[2,8,2,1],[7,2,3,5,6,7]]
  end
end
