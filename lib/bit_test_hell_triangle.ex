defmodule Bit.Tests.Triangle do
  @moduledoc """
   Documentation for Bit.Tests.Triangle.
  """

  def triangle_challenge(multi_array \\ [[6],[3,5],[9,7,1],[4,6,8,4]]) do
    multi_array = multi_array
    |> Enum.map(fn sub_array ->
      Enum.with_index(sub_array)
     end)

    {result,_} = multi_array
    |> Enum.reduce({0,[0,0]}, fn array, tuple_acc ->
        {sum,array_next_indexes} = tuple_acc

        {num1, index1} = array
        |> Enum.at(Enum.at(array_next_indexes,0))

        {num2, index2} = array
        |> Enum.at(Enum.at(array_next_indexes,1))

        tuple_acc = if num1 > num2 do
          {num1 + sum,[index1,index1+1]}
        else
          {num2 + sum,[index2,index2+1]}
        end
     end)

     IO.puts "Maximum total from top to bottom: #{result}"

     result
  end
end
