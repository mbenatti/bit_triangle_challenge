# Problem Description
[HellsTriangle.pdf](https://bitbucket.org/mbenatti/bit_triangle_challenge/raw/8dbf7b3c6025903e5dc97bbcfb0254d58de4eb25/HellsTriangle.pdf)

# Bit.Tests.Triangle

  Used Elixir 1.4.2
  e Erlang 1.9.3
  
  To run the tests enter `mix test` in the project root folder
 
  File test: `./bit_test_hell_triangle/test/bit_test_hell_triangle_test.exs`
  
  Filw with the logic: `./bit_test_hell_triangle/lib/bit_test_hell_triangle.ex`
  
  Function ```Bit.Tests.Triangle.triangle_challenge```
  
  ### How to run a custom test using iex: 
  
    ```
    $ cd lib
    $ iex
    $ Bit.Tests.Triangle.triangle_challenge [[6],[3,5],[9,7,1],[4,6,8,4]]
    ```